#!/bin/bash

# Usage: ./generate-container.sh <container-name>
# Example: ./generate-container.sh julia-frictionless
# Will generate the following directory structure:
# containers/
# ├── julia
# │   └── frictionless
# │       ├── ci.yml
# │       ├── Dockerfile
# │       └── Readme.md

# Get the input container name from the command-line argument
input_string="$1"

# Split the input string into components
IFS='-' read -ra components <<< "$input_string"

# Define the directory path for the new container
container_path="containers/${components[0]}/${components[1]}"

# Create directory structure
mkdir -p "$container_path"
touch "$container_path/ci.yml"
touch "$container_path/Dockerfile"
touch "$container_path/Readme.md"

# Get the current date in the format "YYYY-MM-DD"
current_date=$(date +"%Y-%m-%d")

# Write content to ci.yml
cat > "$container_path/ci.yml" <<EOF
.${input_string}:
  variables:
    CONTAINER_NAME: $input_string
    CONTAINER_PATH: $container_path
    VERSION: 0.1.0

${input_string}-build:
  extends: [ .build-template, .${input_string} ]
EOF

# Write content to Dockerfile
cat > "$container_path/Dockerfile" <<EOF
# Base image
FROM ${components[0]}:latest

# Install additional libraries and dependencies for quality control

# Set the working directory
WORKDIR /usr/src/app

CMD ["julia"]
EOF

# Write content to Readme.md
cat > "$container_path/Readme.md" <<EOF
# ${components[0]}-${components[1]} Container

This Docker container provides executables for the ${components[0]} programming language, along with additional libraries and dependencies to support quality control.

## Container Details
- Maintainer:
- Last Updated: $current_date

## Included Tools

## Description

## Usage Details
EOF

echo "Directory structure, ci.yml, Dockerfile, and Readme.md created for '$input_string'."

# Prompt the user to run the gen-gitlab-ci.sh script
read -p "Do you want to run the gen-gitlab-ci.sh script now? (Y/N): " choice
if [ "$choice" == "Y" ] || [ "$choice" == "y" ]; then
    ./gen-gitlab-ci.sh > .gitlab-ci.yml
    echo "Updated Gitlab CI file generated."
fi
