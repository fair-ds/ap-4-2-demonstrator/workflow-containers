# emacs-vanilla Container

This Docker container provides a vanilla installation of the Emacs text editor.

## Container Details
- Maintainer: Jonathan Hartman
- Last Updated: 2024-05-27

## Included Tools
- `emacs`

## Description

## Usage Details
