# Merge Request: Add New Container Build

## Description

Please provide a brief description of the new container build being added. Include any relevant details about the purpose and functionality of the container.

## Changes

List the changes made in this merge request:

- [ ] Added new container build for [container_name]
- [ ] Updated documentation for the new container

## Verification

Please describe the steps taken to verify the changes. Include any relevant details about testing and validation.

1. [ ] Built the container locally and verified functionality


## Checklist

- [ ] I have read the [contributing guidelines](link_to_contributing_guidelines)
- [ ] I have added necessary documentation (if applicable)
- [ ] I have linked related issues (if applicable)

## Additional Notes

Include any additional notes or comments here.