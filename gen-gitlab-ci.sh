#!/bin/bash

# This script generates the .gitlab-ci.yml file for this project if you're lazy.

echo "stages:"
echo "  - build"
echo
echo "include:"
echo "  - /.gitlab/ci/templates.yml"
find containers -name "ci.yml" | sort | sed 's/^/  - /'