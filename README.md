# FAIR-DS Workflow Containers

## What is this repository?

This repository contains code used to build and deploy containers for the FAIR-DS Catalog for Quality Assurance and Analysis Workflows.
Containers are built weekly and are publically available via this repository's [Container Registry](https://git.rwth-aachen.de/fair-ds/ap-4-2-demonstrator/workflow-containers/container_registry).

**For Questions regarding Usage, Requesting or Contributing Containers, please refer to the [Wiki](https://git.rwth-aachen.de/fair-ds/ap-4-2-demonstrator/workflow-containers/-/wikis/home)**.
